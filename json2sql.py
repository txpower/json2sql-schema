#!/usr/bin/env python2

# COPYRIGHT: 2019 <txpower@muc.ccc.de>
# LICENSE: GNU GPLv2 or later
# DESCRIPTION: create an SQL database schema from a JSON description
# BUGS: no foreign key recursion, many SQL injections (assuming trusted input!)

import json,sys;

db_tables=json.loads(sys.stdin.read())["nodes"]["tables"];

mangle_name=lambda x:x
encode_value=json.dumps
encode_column=lambda x:encode_value(x).replace('"','`').lower()
encode_table=lambda x:encode_value(x).lower()

tables={}

def create_column(left,c):
  out=""
  if c in left.keys():
    for r in left.get(c,{"refs":[]}).get("refs",[]):
      out+=create_column(left,r)
    out+=(left.pop(c)["create"])
  return out

def create_table(left,t):
  out=""
  if t in left.keys():
    for r in left.get(t,{})["refs"].keys():
      out+=create_table(left,r)
    out+=(left.pop(t)["create"])
  return out;

for table in db_tables.keys():
  ui={}
  t_out="\nCREATE TABLE IF NOT EXISTS "+encode_table(table)+"(\n    `id`\n        INTEGER"
  t_out+="\n        NOT NULL\n        PRIMARY KEY\n        AUTOINCREMENT"
  columns={}
  tables[table]={"refs":{}}
  for column in db_tables[table].keys():
    columns[column]={}
    c_out=",\n    "+encode_column(column)
    c_out+="\n        "+mangle_name(db_tables[table][column].get("type","integer")).upper()
    k=db_tables[table][column].keys()
    if "unique" in k:
      for u in db_tables[table][column]["unique"].keys():
        if len(u):
          if not u in ui.keys():
            ui[u]={}
          ui[u][column]=[db_tables[table][column]["unique"][u]]
        else:
          c_out+="\n        UNIQUE"
    if "check" in k:
      ref=db_tables[table][column]["check"].get("ref",[])
      columns[column]["refs"]=ref
      ref=[column]+ref
      sql="";
      c_out+="\n        CHECK(\n            "
      for x in db_tables[table][column]["check"].get("sql",[]):
        if type(x) in [int,long]:
          c_out+=encode_column(ref[x])
        elif type(x) in [bytes,unicode]:
          c_out+=x.replace("\n","\n            ")
      c_out+="\n        )"
    if not db_tables[table][column].get("null",0):
      c_out+="\n        NOT NULL"
    if "default" in k:
      c_out+="\n        DEFAULT "+encode_value(db_tables[table][column]["default"])
    if "references" in k:
      r=db_tables[table][column]["references"]
      c_out+="\n        REFERENCES "+encode_table(r)+"(`id`)"
      tables[table]["refs"][r]="id"
    columns[column]["create"]=c_out
  for c in columns.keys():
      t_out+=create_column(columns,c)
  t_out+="\n);"
  for u in ui.keys():
    t_out+="\nCREATE UNIQUE INDEX IF NOT EXISTS "+encode_table(table+"__"+u)
    t_out+="\nON "+encode_table(table)+"(\n        "
    t_out+=",\n    ".join(encode_column(c) for c in ui[u].keys())
    t_out+="\n);"
  t_out+="\n"
  tables[table]["create"]=t_out

for t in tables.keys():
  sys.stdout.write(create_table(tables,t))
